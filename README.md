# Requirements :
- npm / Node,
- composer,
- openWeather Api Key

# installation :

install composer dependencies:

```
composer install 
```

install node dependencies:
```
npm install
```
- copy .env.example as .env


Generate Laravel Key
```
php artisan key:generate 
```

- add an OPEN_WEATHER_APP_ID  key in the .env (my openweather api key = cb86b6095c50433c95999f7be1de2998)

Run the server:
```
php artisan serve
```

# Usage

## Web :
go to address:
``` 
/weather
```
choose your city from the drop down
The next five or six days weather forecast will be displayed

## Console:

Type in : 
```
php artisan weatherReport canberra,brisbane 
```
The forecast for next five or six days will be displayed for each city
In case, The city does not exist or there was no connection the message will be displayed in front of the city




# Recommendation :

It is better for the list of the city to be populated by a better source other than config file.
 
 WeatherInfo package response is better to have the implementation of Interface or an Abstract class that different drivers would use those classes for class responses.

Api calls to third party is better to be done with another service.

# Limitation :
For sake of this test,I have used openweather for api calls. Free version of this service only has 40 instances of weather forecast which means 5 to 6 incomplete days .

 This means that first day or the sixth day has incomplete hours and the average would result in inaccurate results

# Assumption :
The cities meant to be the major cities in Australia