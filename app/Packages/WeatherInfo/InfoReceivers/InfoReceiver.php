<?php

namespace App\Packages\WeatherInfo\infoReceivers;

use App\Packages\WeatherInfo\Contracts\InfoReceiving\InfoReceiver as InfoReceiverContract;

abstract class InfoReceiver implements InfoReceiverContract
{
    protected $city_name;

    public function setCityName(string $city_name)
    {
        $this->city_name = $city_name;
    }

}