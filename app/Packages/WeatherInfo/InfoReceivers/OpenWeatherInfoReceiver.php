<?php

namespace App\Packages\WeatherInfo\infoReceivers;

use Exception;

class OpenWeatherInfoReceiver extends InfoReceiver
{

    private $app_id;
    private $api_address;
    private $five_days_address;
    private $country_code;
    private $unit;


    public function __construct(array $config)
    {
        $this->app_id               = $config['app_id'];
        $this->api_address          = $config['api_address'];
        $this->five_days_address    = $config['five_days_address'];
        $this->country_code         = $config['country_code'];
        $this->unit                 = $config['unit'];
    }


    /**
     * @return array
     * @throws Exception
     */
    public function getFiveDaysResult()
    {

        $result = $this->getFiveDaysResultByCity($this->city_name);

        $this->calculateDailyFromFiveDaysResult($result);

        return $this->mapResult($result);
    }

    public function mapResult($result)
    {
        $mapped_result = [];

        foreach ($result->days as $date => $day) {
            $mapped_result[] = [
                'date'                  =>  $date,
                'average_temperature'   =>  $day['average']
            ];
        }

        return $mapped_result;
    }

    private function buildUrlWithCityName(string $city_name)
    {
        $data = [
            'q'     =>  $city_name.','.$this->country_code,
            'units' =>  $this->unit,
            'APPID' =>  $this->app_id,
        ];

        return sprintf("%s?%s", $this->api_address.$this->five_days_address, http_build_query($data));
    }

    /**
     * @param $city_name
     * @return mixed
     * @throws Exception
     */
    public function getFiveDaysResultByCity($city_name)
    {
        $url = $this->buildUrlWithCityName($city_name);

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $url);

        $result = curl_exec($curl);

        if ($result === false) {
            $this->throwCurlException($curl);
        }

        if (!$result) {
            throw new Exception('Connection Failure');
        }

        curl_close($curl);

        $result = json_decode($result);

        $this->validateResult($result);

        return $result;
    }

    private function calculateDailyFromFiveDaysResult($result)
    {
        $result->days = [];

        date_default_timezone_set('Australia/Brisbane');

        foreach ($result->list as $index => $list) {
            $list_date = date('d/m/Y', $list->dt);

            $three_hours_total = array_key_exists($list_date, $result->days)
                ? $result->days[$list_date]['three_hours_total']
                : 0 ;

            $new_three_hours_total = $three_hours_total + $list->main->temp;

            $result->days[$list_date]['three_hours'][]       = $list->main->temp;
            $result->days[$list_date]['three_hours_total']   = $new_three_hours_total;

            $average = $new_three_hours_total / count($result->days[$list_date]['three_hours']);

            $result->days[$list_date]['average'] = round($average);
        }
    }

    /**
     * @param $result
     * @throws Exception
     */
    private function validateResult($result)
    {
        if ($result->message) {
            throw new Exception($result->message);
        }
    }

    /**
     * @param $curl
     * @throws Exception
     */
    private function throwCurlException($curl)
    {
        $errno = curl_errno($curl);

        $error_message = curl_strerror($errno);

        curl_close($curl);

        throw new Exception($error_message);
    }
}