<?php

namespace App\Packages\WeatherInfo;

use InvalidArgumentException;
use App\Packages\WeatherInfo\infoReceivers\OpenWeatherInfoReceiver;

class WeatherInfoManager
{
    protected $drivers;
    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    private function driver($name = null)
    {
        $name = $name ?: $this->getDefaultDriver();

        return $this->drivers[$name] = $this->get($name);
    }

    protected function get($name)
    {
        return $this->drivers[$name] ?? $this->resolve($name);
    }

    protected function resolve($name)
    {
        $config = $this->getConfig($name);


        $driverMethod = 'create'.ucfirst($config['driver']).'Driver';

        if (! method_exists($this, $driverMethod)) {
            throw new InvalidArgumentException("Driver [{$config['driver']}] is not supported.");
        }

        return $this->{$driverMethod}($config);
    }

    protected function getConfig($name)
    {
        if (! is_null($name) && $name !== 'null') {
            return $this->app['config']["weatherApi.connections.{$name}"];
        }

        return ['driver' => 'null'];
    }

    protected function getDefaultDriver()
    {
        return $this->app['config']['weatherApi.default'];
    }


    private function createOpenWeatherDriver(array $config)
    {
        return new OpenWeatherInfoReceiver($config);
    }


    public function __call($method, $parameters)
    {
        return $this->driver()->$method(...$parameters);
    }
}