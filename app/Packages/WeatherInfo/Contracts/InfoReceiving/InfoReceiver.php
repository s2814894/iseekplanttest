<?php

namespace App\Packages\WeatherInfo\Contracts\InfoReceiving;

interface InfoReceiver
{
    public function setCityName(string $city_name);

    public function getFiveDaysResult();

    public function mapResult($result);

}