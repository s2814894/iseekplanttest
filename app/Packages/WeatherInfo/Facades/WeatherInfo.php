<?php

namespace App\Packages\WeatherInfo\Facades;

use Illuminate\Support\Facades\Facade;

class WeatherInfo extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'WeatherInfo';
    }
}