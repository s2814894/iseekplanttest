<?php

namespace App\Packages\ApiResponse;


class ApiResponseManager
{
    private $response = [];
    private $errors = [];
    private $result = 'success';
    private $error;

    public static function create()
    {
        return new static();
    }

    public function addError(string $error)
    {
        $this->setResultFailed();

        $this->errors[] = $error;

        return $this;
    }

    public function setError(string $error)
    {
        $this->setResultFailed();

        $this->error = $error;

        return $this;
    }

    public function setResponse(array $response)
    {
        $this->response = $response;

        return $this;
    }

    private function setResultFailed()
    {
        $this->result = 'failed';

        return $this;
    }

    public function toArray()
    {
        return [
            'response'       =>  $this->response,
            'errors'         =>  $this->errors,
            'error'          =>  $this->error,
            'result'         =>  $this->result,
        ];
    }
}