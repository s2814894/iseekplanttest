<?php

namespace App\Console\Commands;

use App\Packages\WeatherInfo\Facades\WeatherInfo;
use Illuminate\Console\Command;

class WeatherReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weatherReport {cities}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reports  5 - 6  days forecast for multiple cities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(WeatherInfo $weatherInfo)
    {
        //
        $cities = explode(',',$this->argument('cities'));

        foreach ($cities as $city) {

            $weatherInfo::setCityName($city);

            try {
                echo $city . ':';

                foreach (WeatherInfo::getFiveDaysResult() as $result){

                    echo $result['average_temperature'] . ' ';
                }

            } catch (\Exception $e) {

                echo  $e->getMessage();

            }

            echo "\n";
        }
    }
}
