<?php

namespace App\Http\Controllers;

use App\Packages\ApiResponse\Facades\ApiResponse;
use App\Packages\WeatherInfo\Facades\WeatherInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class WeatherController extends Controller
{
    //

    public function show()
    {
        return view('weather');
    }

    public function getByCity(Request $request)
    {
        $request->validate([
            'city_name' => 'required',
        ]);

        WeatherInfo::setCityName($request->city_name);

        try {
            $five_days_result = WeatherInfo::getFiveDaysResult();


            ApiResponse::setResponse($five_days_result);

        } catch (\Exception $e) {
            ApiResponse::setError($e->getMessage());

        }

        return ApiResponse::toArray();
    }

    public function getCities()
    {
        return ApiResponse::setResponse(Config::get('cities'))->toArray();
    }
}
