<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use App\Packages\WeatherInfo\WeatherInfoManager;

class WeatherInfoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('WeatherInfo', function () {
            return new WeatherInfoManager($this->app);
        });
    }


}
