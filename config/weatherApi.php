<?php

return [

    'default' => env('WEATHER_DRIVER', 'openWeather'),

    'connections' => [
        'openWeather' => [
            'driver'            =>  'openWeather',
            'app_id'            =>  env('OPEN_WEATHER_APP_ID'),
            'api_address'       =>  'http://api.openweathermap.org/data/2.5',
            'five_days_address' =>  '/forecast',
            'country_code'      =>  'au',
            'unit'              =>  'metric'
        ],
    ],

];
