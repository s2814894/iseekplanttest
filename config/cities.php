<?php

return [
    [
        'name'    =>  'Albury-Wodonga',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Armidale',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Ballina',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Balranald',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Batemans Bay',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Bathurst',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Bega',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Bourke',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Bowral',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Broken Hill',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Byron Bay',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Camden',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Campbelltown',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Cobar',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Coffs Harbour',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Cooma',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Coonabarabran',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Coonamble',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Cootamundra',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Corowa',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Cowra',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Deniliquin',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Dubbo',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Forbes',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Forster',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Glen Innes',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Gosford',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Goulburn',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Grafton',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Griffith',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Gundagai',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Gunnedah',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Hay',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Inverell',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Junee',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Katoomba',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Kempsey',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Kiama',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Kurri Kurri',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Lake Cargelligo',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Lismore',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Lithgow',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Maitland',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Moree',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Moruya',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Murwillumbah',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Muswellbrook',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Nambucca Heads',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Narrabri',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Narrandera',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Newcastle',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Nowra-Bomaderry',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Orange',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Parkes',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Parramatta',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Penrith',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Port Macquarie',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Queanbeyan',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Raymond Terrace',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Richmond',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Scone',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Singleton',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Sydney',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Tamworth',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Taree',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Temora',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Tenterfield',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Tumut',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Ulladulla',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Wagga Wagga',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Wauchope',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Wellington',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'West Wyalong',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Windsor',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Wollongong',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Wyong',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Yass',
        'state' =>  'New South Wales'
    ],
    [
        'name'    =>  'Young',
        'state' =>  'New South Wales'
    ],
    [
        'name'  =>  'Alice Springs',
        'state'  =>'Northern Territory'
    ],
    [
        'name'  =>  'Anthony Lagoon',
        'state' =>'Northern Territory'
    ],
    [
        'name'  =>  'Darwin',
        'state' =>'Northern Territory'
    ],
    [
        'name'  =>  'Katherine',
        'state' =>'Northern Territory'
    ],
    [
        'name'  =>  'Tennant Creek',
        'state' =>'Northern Territory'

    ],
    [
        'name' =>  'Ayr',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Beaudesert',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Blackwater',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Bowen',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Brisbane',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Buderim',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Bundaberg',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Caboolture',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Cairns',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Charleville',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Charters Towers',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Cooktown',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Dalby',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Deception Bay',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Emerald',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Gatton',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Gladstone',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Gold Coast',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Goondiwindi',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Gympie',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Hervey Bay',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Ingham',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Innisfail',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Kingaroy',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Mackay',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Mareeba',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Maroochydore',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Maryborough',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Moonie',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Moranbah',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Mount Isa',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Mount Morgan',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Moura',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Redcliffe',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Rockhampton',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Roma',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Stanthorpe',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Toowoomba',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Townsville',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Warwick',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Weipa',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Winton',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Yeppoon',
        'state' =>  'Queensland'
    ],
    [
        'name' =>  'Adelaide',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Ceduna',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Clare',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Coober Pedy',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Gawler',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Goolwa',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Iron Knob',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Leigh Creek',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Loxton',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Millicent',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Mount Gambier',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Murray Bridge',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Naracoorte',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Oodnadatta',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Port Adelaide Enfield',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Port Augusta',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Port Lincoln',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Port Pirie',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Renmark',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Victor Harbor',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Whyalla',
        'state' =>  'South Australia',
    ],
    [
        'name' =>  'Beaconsfield',
        'state' =>  'Tasmania'
    ],
    [
        'name' =>  'Bell Bay',
        'state' =>  'Tasmania'
    ],
    [
        'name' =>  'Burnie',
        'state' =>  'Tasmania'
    ],
    [
        'name' =>  'Devonport',
        'state' =>  'Tasmania'
    ],
    [
        'name' =>  'Hobart',
        'state' =>  'Tasmania'
    ],
    [
        'name' =>  'Kingston',
        'state' =>  'Tasmania'
    ],
    [
        'name' =>  'Launceston',
        'state' =>  'Tasmania'
    ],
    [
        'name' =>  'New Norfolk',
        'state' =>  'Tasmania'
    ],
    [
        'name' =>  'Queenstown',
        'state' =>  'Tasmania'
    ],
    [
        'name' =>  'Richmond',
        'state' =>  'Tasmania'
    ],
    [
        'name' =>  'Rosebery',
        'state' =>  'Tasmania'
    ],
    [
        'name' =>  'Smithton',
        'state' =>  'Tasmania'
    ],
    [
        'name' =>  'Stanley',
        'state' =>  'Tasmania'
    ],
    [
        'name' =>  'Ulverstone',
        'state' =>  'Tasmania'
    ],
    [
        'name' =>  'Wynyard',
        'state' =>  'Tasmania'
    ],
    [
        'name'  =>  'Albury-Wodonga',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Ararat',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Bacchus Marsh',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Bairnsdale',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Ballarat',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Beechworth',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Benalla',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Bendigo',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Castlemaine',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Colac',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Echuca',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Geelong',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Hamilton',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Healesville',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Horsham',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Kerang',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Kyabram',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Kyneton',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Lakes Entrance',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Maryborough',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Melbourne',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Mildura',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Moe',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Morwell',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Port Fairy',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Portland',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Sale',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Sea Lake',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Seymour',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Shepparton',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Sunbury',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Swan Hill',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Traralgon',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Yarrawonga',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Wangaratta',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Warragul',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Werribee',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Wonthaggi',
        'state' =>  'Victoria'
    ],
    [
        'name'  =>  'Broome',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Bunbury',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Busselton',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Coolgardie',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Dampier',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Derby',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Fremantle',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Geraldton',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Kalgoorlie',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Kambalda',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Katanning',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Kwinana',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Mandurah',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Meekatharra',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Mount Barker',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Narrogin',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Newman',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Northam',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Perth',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Port Hedland',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Tom Price',
        'state' =>  'Western Australia'
    ],
    [
        'name'  =>  'Wyndham',
        'state' =>  'Western Australia'
    ],

];
