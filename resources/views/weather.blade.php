<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"  href="{{asset('css/app.css')}}">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

</head>
<body>
<div class="container md:w-3/4 p-1 lg:w-3/4 mx-auto">
    <div id="weather">
    </div>
</div>
</body>
<script src="{{asset('js/app.js')}}"></script>
</html>
