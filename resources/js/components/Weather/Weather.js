import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import CityList from "./CityList";
import Forecast from "./Forecast";
import axios from "axios/index";

export default class Weather extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected_city : null,
            city_is_selected : false,
            forecast : [],
            loading : false,
            error : ''
        };
        this.handleCityChange = selectedOption => {
            const selected_city = selectedOption;
            const city_is_selected = true;
            this.setState({ city_is_selected });
            this.setState({ selected_city });
            this.setState({ error:'' });
            this.setState({ loading:true });


            axios.get(`/api/weather/get-by-city`,{
                params: {
                    city_name : selected_city.value
                }
            })
            .then(res => {
                if(res.data.result == 'success')
                {
                    const forecast = res.data.response;
                    this.setState({ forecast });
                }else{
                    this.setState({ error:res.data.error});

                }
            }).catch(error => {
                if (error.response.status === 422){
                    const response_errors = error.response.data.errors;

                    const errors = Object.keys(response_errors).map(function (error, index) {
                        return <div> {response_errors[error]} </div> ;
                    });

                    this.setState({ error: errors});

                } else {
                    console.log(error.response);
                }
            }).finally( () => {
                this.setState({ loading:false });
            })

        };
    }

    componentDidMount() {
    }

    render() {
        return (
            <div className="mt-5">
                <div className ="text-2xl text-black">
                    Choose a city:
                </div>
                <CityList handleCityChange={this.handleCityChange}/>
                {
                    this.state.loading
                        ?
                        <div className="loader">LOADING..</div>
                        :

                            this.state.error === ''
                            ?
                                <div className="relative mx-auto text-center">
                                    {
                                        this.state.city_is_selected
                                            ?

                                            this.state.forecast.map(function (forecast, index) {
                                                return <Forecast forecast={forecast} key={index}/> ;
                                            })

                                            :
                                            ''
                                    }
                                </div>
                            :
                            <div className="alert bg-red-300 rounded p-5 m-5 ">
                                {this.state.error}
                            </div>



                }


            </div>
        );
    }
}

if (document.getElementById('weather')) {
    ReactDOM.render(<Weather />, document.getElementById('weather'));
}
