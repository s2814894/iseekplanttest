import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Select from 'react-select';

export default class CityList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            city_list: [],
            selected_option: null,
            options : []
        };
        this.handleChange = selectedOption => {
            const selected_option = selectedOption;
            this.setState({ selected_option });
            props.handleCityChange(selectedOption);

        };
    }

    componentDidMount() {
        axios.get(`/api/weather/list/cities`)
            .then(res => {
                const city_list = res.data.response;
                const options = res.data.response.map(city => ({value: city.name , label: city.name + ' ' + city.state}));
                this.setState({ city_list });
                this.setState({ options });
            })
    }



    render() {
        return (
            <div className="mt-4">
                <Select
                    value={this.state.selected_option}
                    onChange={this.handleChange}
                    options={this.state.options}
                />

            </div>
        );
    }
}

if (document.getElementById('cityList')) {
    ReactDOM.render(<CityList />, document.getElementById('cityList'));
}
