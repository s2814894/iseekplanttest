import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Forecast extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {

    }


    render() {
        return (
            <div className="inline-block p-10  m-5 border-grey-500 bg-gray-200  border-solid border-2">
                <div className="mx-auto text-center font-size text-6xl">
                    {this.props.forecast.average_temperature}C
                </div>
                <div className="mx-auto text-center">
                    {this.props.forecast.date}
                </div>

            </div>
        );
    }
}

if (document.getElementById('forecast')) {
    ReactDOM.render(<Forecast />, document.getElementById('forecast'));
}
