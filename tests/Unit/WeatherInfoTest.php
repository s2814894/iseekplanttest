<?php

namespace Tests\Unit;

use App\Packages\WeatherInfo\Facades\WeatherInfo;
use App\Packages\WeatherInfo\WeatherInfoManager;
use Tests\TestCase;

class WeatherInfoTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function testCreatingWeatherInfoManager()
    {
        $this->createWeatherInfoManager();
        $this->assertTrue(true);
    }

    public function testSettingWeatherInfoManagerCity()
    {
        $weatherInfoManager = $this->createWeatherInfoManager();
        $this->setValidCity($weatherInfoManager);
        $this->assertTrue(true);
    }

    public function testGetFiveDaysForecast()
    {
        $weatherInfoManager = $this->createWeatherInfoManager();
        $this->setValidCity($weatherInfoManager);
        $this->getFiveDaysResult($weatherInfoManager);
        $this->assertTrue(true);
    }

    public function testGetFiveDaysForecastWithInvalidCity()
    {
        $weatherInfoManager = $this->createWeatherInfoManager();
        $this->setInvalidCity($weatherInfoManager);
        try {
            $this->getFiveDaysResult($weatherInfoManager);
            $this->assertTrue(false);
        } catch (\Exception $exception) {
            $this->assertTrue($exception->getMessage() == 'city not found');
        }
    }

    public function testGetFiveDaysForecastResult()
    {
        $weatherInfoManager = $this->createWeatherInfoManager();
        $this->setValidCity($weatherInfoManager);
        $results = $this->getFiveDaysResult($weatherInfoManager);
        $this->assertTrue(count($results) >= 5);
        array_map(function ($result) {
            if (!isset($result['date'])) {
                $this->assertTrue(false);
            } else {
                $this->assertTrue(\DateTime::createFromFormat('d/m/Y', $result['date']) !== false);
            }

            if (!isset($result['average_temperature'])) {
                $this->assertTrue(false);
            } else {
                $this->assertTrue(is_numeric($result['average_temperature']));
            }
        }, $results);
    }

    public function testCreatingWeatherInfoFacade()
    {
        $this->createWeatherInfoFacade();
        $this->assertTrue(true);
    }

    public function testCreatingWeatherInfoFiveDaysResultFacade()
    {
        $this->createWeatherInfoFacade();
        $result = $this->createFiveDaysResultFacade();
        $this->assertTrue(!empty($result));
    }

    private function createWeatherInfoManager()
    {
        return new WeatherInfoManager($this->app);
    }

    private function setValidCity(WeatherInfoManager $weatherInfoManager)
    {
        $weatherInfoManager->setCityName('brisbane');
    }

    private function getFiveDaysResult(WeatherInfoManager $weatherInfoManager)
    {
        return $weatherInfoManager->getFiveDaysResult();
    }

    private function setInvalidCity($weatherInfoManager)
    {
        $weatherInfoManager->setCityName('INVALID CITY!!!');
    }

    private function createWeatherInfoFacade()
    {
        WeatherInfo::setCityName('brisbane');
    }

    private function createFiveDaysResultFacade()
    {
        return WeatherInfo::getFiveDaysResult('brisbane');
    }
}
