<?php

namespace Tests\Feature;


use Tests\TestCase;

class WeatherInfo extends TestCase
{

    public function testWeatherUrl()
    {
        $response = $this->get('/weather');
        $response->assertStatus(200);
    }

    public function testCitiesListUrl()
    {
        $response = $this->get('api/weather/list/cities');
        $response->assertStatus(200);
        $response->assertJsonCount(4);
        $response->assertJson(['result' =>  'success']);
    }

    public function testWeatherFiveDaysResultUrl()
    {
        $response = $this->json('get', 'api/weather/get-by-city', ['city_name' => 'Brisbane']);
        $response->assertStatus(200);
        $response->assertJsonCount(4);
        $response->assertJson(['result' =>  'success']);
    }
    public function testWeatherFiveDaysResultUrlWithoutCityName()
    {
        $response = $this->json('get', 'api/weather/get-by-city', ['not_city_name' => 'Brisbane']);
        $response->assertStatus(422);
        $response->assertJson(['errors' =>  ['city_name' => ['The city name field is required.']]]);
    }

}
